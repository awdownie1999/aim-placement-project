<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organisation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'type',

    ];
    
    public function site()
    {
        return $this->hasMany('App\Site');
    }
    /* 
    protected $table = 'organisations';
    protected $primaryKey = 'id';

    public function site()
    {
        return $this->hasMany('Site', 'id');
    }
*/
}


   
