<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Organisation;
use App\Providers\RouteServiceProvider;

use Illuminate\Http\Request;

use Illuminate\Auth\Events\Registered;

use App\User;
use Facade\FlareClient\View;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    /**$sites = Site::all();
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'contact_num' => ['required', 'string', 'max:11'],
            'contact_two' => ['string', 'max:11'],
            'address' => ['required', 'string'],
            'address_two' => ['string']

        ]);
    }

    public function showRegistrationForm()
    {
        $organisations = Organisation::all();
        return view('auth.register', compact('organisations'));
    }

    public function register(Request $request)
    {

        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'contact_num' => $data['contact_num'],
            'contact_two' => $data['contact_two'],
            'address' => $data['address'],
            'address_two' => $data['address_two'],
            'level_of_access' => 1,

        ]);

        return $user;
    }
}
