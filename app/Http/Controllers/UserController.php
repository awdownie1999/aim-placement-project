<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Log;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return View('user.indexUser', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        return View('user.createUser');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = User::firstOrNew([
            'email' => $request['email'],
        ]);


        $userRole = $request->get('user_role');


        Log::info("USER Roles Logging " . $userRole);
        $roles = Role::where('user_role', $userRole)->first();

        Log::info("Roles Logging" . $roles);

        $level_of_access = $roles->id;

        $user->fill([
            'name' => $request->name,
            'password' => Hash::make($request->password),
            'address' => $request->address,
            'address_two' => $request->address_two,
            'contact_num' => $request->contact_num,
            'contact_two' => $request->contact_two,
            'level_of_access' => $level_of_access,
            'available' => $request->available,
            'hours' => $request->hours,
        ]);
        $user->save();

        return redirect('/users')->with('success', 'User saved');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('user.editUser', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = Hash::make($request->get('password'));
        $user->address = $request->get('address');
        $user->address_two = $request->get('address_two');
        $user->contact_num = $request->get('contact_num');
        $user->contact_two = $request->get('contact_two');
        //TODO change to role Select in blade.
        $user->level_of_access = $request->get('level_of_access');
        $user->available = $request->get('available');
        $user->hours = $request->get('hours');

        $user->save();


        return redirect('/users')->with('success', 'The User has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $user = User::find($id);
        $user->delete();

        return redirect('/users')->with('success', 'User Deleted');
    }
}
