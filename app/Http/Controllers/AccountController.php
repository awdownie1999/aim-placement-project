<?php

namespace App\Http\Controllers;

use App\Organisation;
use Illuminate\Http\Request;

use App\Role;
use App\Site;
use App\SiteUser;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class AccountController extends Controller
{

    /* Index of the Account page will check for the role of the user before displaying any extra information */
    public function index()
    {

        //Log::info(Auth::user());

        $user = Role::find(Auth::user()->level_of_access);
        $role = $user->user_role;
        $site_user = SiteUser::all();


        Log::info(PHP_EOL . PHP_EOL . '[Account Controller] Role: ' . $role . PHP_EOL . PHP_EOL);

        /*Case insensitivly compare the strings, == 0 when they are the same*/
        if (strcasecmp($role, 'Client') == 0) {


            $site = null;
            $organisation = null;
            foreach ($site_user as $siteTemp) {
                if ($siteTemp->user_id == Auth::user()->id) {
                    $site = Site::find($siteTemp->site_id);
                }
            }


            if ($site != null) {
                $organisation = Organisation::find($site->organisation_id);
            }

            return View('account.account', compact('user', 'role', 'site_user', 'site', 'organisation'));
        }
        return View('account.account', compact('user', 'role', 'site_user'));
    }

    public function edit($id)
    {

        $user_id = Auth::user()->id;
        $site_user = SiteUser::where('user_id', $user_id)->first();

        $user_site_id = null;

        if ($site_user != null) {
            $user_site_id = $site_user->id;
        }


        Log::info('User ID from Account Controller EDIT : ' . $user_id);
        Log::info('Site User ID from Account Controller EDIT : ' . $user_site_id);
        $organisations = Organisation::all();
        $sites = Site::all();

        return View('account.changeSite', compact('user_id', 'user_site_id', 'organisations', 'sites'));
    }

    public function show()
    {
    }

    public function getOrganisations()
    {
        $data = Organisation::get();

        //        Log::info('Information TO Log Organisation : ' . $data);

        return response()->json($data);
    }


    public function getSites(Request $request)
    {

        //Log::info('Organisation ID ' . $request->organisation_id);
        $data = Site::where('organisation_id', $request->organisation_id)->get();

        return response()->json($data);
    }

    public function updateSites(Request $request)
    {




        $siteUser = SiteUser::find($request->site_user_id);

        if ($siteUser == null) {

            Log::info('');

            $siteUser = SiteUser::firstOrNew([
                'user_id' => $request->user_id,
            ]);

            $siteUser->fill([
                'site_id' => $request->site_id
            ]);

            $siteUser->save();
        } else {






            Log::info('User Found : ' . $siteUser);
            Log::info('Organisation ID Found : ' . $request->organisation_id);
            Log::info('Site ID Found : ' . $request->site_id);


            $siteUser->site_id = $request->site_id;

            $siteUser->save();
        }


        return redirect('account'); //TODO add success message
    }
}
