<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Organisation;
use App\Site;
use Log;

class OrganisationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $organisations = Organisation::all();
        return view('organisation.viewOrganisations', compact('organisations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $organisations = Organisation::all();
        return view('organisation.createOrganisation', compact('organisations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            
        ]);
        $organisations = new Organisation([ 
            
            'name' => $request->get('name'), 
            'type' => $request->get('type'),
        ]);
        $organisations->save();
        
        
        return redirect('organisation')->with('success', 'organisation saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $organisations = Organisation::find($id);
        return view('organisation.editOrganisation', compact('organisations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $organisations = Organisation::find($id);
        $organisations->name = $request->get('name');
        $organisations->type = $request->get('type');
        
        $organisations->save();

        return redirect('/organisation')->with('success', 'The site has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $organisations = Organisation::find($id);
        $organisations->delete();

        return redirect('/organisation')->with('success', 'site deleted!');
    }
}
