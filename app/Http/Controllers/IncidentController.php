<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Incident;
use Log;
use App\Organisation;
use App\Site;
use App\Job;

class IncidentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $organisations = Organisation::all();
        $incidents = Incident::all();
        return view('incident.incidentIndex', compact('incidents', 'organisations'));
    }


    public function incident(Request $request)
    {
        $id = $request->input('id');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        //$organisation = Organisation::all();
        //$var = $request->get('organisation_id');
        //$sites = Site::where('organisation_id', $var)
        //->get();
        $organisations = Organisation::all();
        //$var = $request->get('organisation_id');
        //$sites = Site::where('organisation_id', $var)
        //->get();
        $sites = Site::all();
        return view('incident.createIncident', compact('sites', 'organisations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'severity' => 'required', 
        ]);       

        $incident = new Incident([
        
            'severity' => $request->get('severity'),
            'name' => $request->get('name'),
           
        ]);
            
        Log::info('This is the check of incident var:' . $incident);
        Log::info('This is the check of incident var name:' . $incident->get('name'));
        Log::info('This is the check of name:' . $incident->name);
        Log::info('This is the request check:' . $request->get('name'));
        Log::info($incident);

        $site = Site::find($request->get('site_id'));
        $organisation = Organisation::find($request->get('organisation_id'));

        $incident->site()->associate($site);
        $incident->save();
        

        return redirect('home')->with('success', 'A manager will look at your request shortly');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $incidents = Incident::find($id);
        return view('incident.displayOneIncident', compact('incidents'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $incidents = Incident::find($id);
        Log::info('IncidentController Edit');
        return view('incident.editIncident', compact('incidents'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $incidents = Incident::find($id);
        $incidents->severity = $request->get('severity');
        $incidents->save();

        return redirect('/incidents')->with('success', 'The incident has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $incidents = Incident::find($id);
        $incidents->delete();

        return redirect('/incidents')->with('success', 'incident deleted!');
    }
}
