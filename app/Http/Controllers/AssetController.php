<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Asset;
use App\Category;
use Log;

class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assets = Asset::all();
        $categories = Category::all();
        Log::info('AssetController Index');
        return view('assets.assetIndex', compact('assets', 'categories'));
    } 

    /**
     * Show the form for creating a new resource.
     *asset_code", "name", "cost", "category_type
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        Log::info('AssetController Create');
        return view('assets.createAsset', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log::info('AssetController Store');

        $request->validate([
            'asset_code'=>'required',
            'name' => 'required',
            'cost' => 'required',
            
        ]);
        Log::info($request);
        $asset = new Asset([
            'asset_code' => $request->get('asset_code'),
            'name' => $request->get('name'),
            'cost' => $request->get('cost'),
        ]);
        Log::error($request->get('category_id'));
        $categories = Category::find($request->get('category_id'));
        Log::error($categories);
        $asset->category()->associate($categories);
        $asset->save();
        return redirect('assets')->with('success', 'asset saved');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $assets = Asset::find($id);
        return view('assets.displayOneAsset', compact('assets'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::all();
        $assets = Asset::find($id);
        Log::info('AssetController Edit');
        return view('assets.editAsset', compact('categories', 'assets'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
{
        $assets = Asset::find($id);
        $categories = Category::find($id);
        $assets->asset_code =  $request->get('asset_code');
        $assets->name = $request->get('name');
        $assets->cost = $request->get('cost');
        $categories = Category::find($request->get('category_id'));
        $assets->save();

        return redirect('/assets')->with('success', 'The asset has been updated!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $assets = Asset::find($id);
        $assets->delete();

        return redirect('/assets')->with('success', 'asset deleted!');
    }
}
