<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Organisation;
use App\Site;
use Log;

class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) // Pass in parameter to filter
    {
        Log::info($request);
        $organisation = Organisation::find($request->get('organisation_id'));
        $var = $request->get('organisation_id');
        $sites = Site::where('organisation_id', $var)
            ->orderBy('location', 'desc')
            ->take(10)
            ->get();
        return view('organisation.viewSites', compact('sites', 'organisation'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        Log::info($request);
        $organisations = Organisation::all();
        $var = $request->get('organisation_id');
        $sites = Site::where('organisation_id', $var)
            ->get();
        return view('organisation.createSite', compact('organisations', 'sites'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log::info('Site Data Stored');
        $request->validate([
            'location' => 'required',

        ]);
        //Log::info($request);
        $site = new Site([
            //Depending on organisation -> get Organisation_ID
            'location' => $request->get('location'),
            'contact_num' => $request->get('contact_num'),
            'manager_contact_number' => $request->get('contact_number'), //Return Manager of Store Number Do as below
        ]);
        Log::error($request->get('organisation_id'));
        $organisation = Organisation::find($request->get('organisation_id'));
        Log::error($organisation);
        $site->organisation()->associate($organisation);
        $site->save();

        return redirect('/organisation')->with('success', 'sites saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Needs to filter by organisation ID
        $organisations = Organisation::all();
        $sites = Site::all();
        return view('organisation.viewSites', compact('organisations', 'sites'));
        //$sites = Organisation::with('sites')->find($id)->sites;
        //return View::make('organisation.viewSites')->with('sites', $sites);  /// This uses org id  to display all sites with an org
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $site = Site::find($id);
        return view('organisation.editSite', compact('site'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sites = Site::find($id);
        $sites->location = $request->get('location');
        $sites->contact_num = $request->get('contact_num');
        $sites->save();

        return redirect('/organisation')->with('success', 'The site has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sites = Site::find($id);
        $sites->delete();

        return redirect('/organisation')->with('success', 'site deleted!');
    }
}
