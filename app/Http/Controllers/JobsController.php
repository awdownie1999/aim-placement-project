<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Incident;
use App\User;
use App\Organisation;
use App\Job;
use App\Site;
use App\Role;
use App\Status;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Log;

class JobsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request) 
    {
        if($request->get('status')  == NULL) {
            $jobs =Job::all();
        }else {
            $jobs = Job::where('status', $request->get('status'))->get();
        }        
        $current_user = Auth::user();

        if ($current_user->level_of_access == 2) {
            $newJobs = array();
            foreach ($jobs as $job) {
                if ($job->user == $current_user) {
                    array_push($newJobs, $job);
                }
            }
            $jobs = $newJobs;
        }
        return View('jobs.jobIndex', compact('jobs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $organisation = Organisation::all();
        $sites = Site::all();
        $incidents = Incident::all();
        return view('jobs.createJobs', compact('organisation', 'sites', 'incidents'));
    }
    /**Stores
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*** Creation of JOB obj from incident ***/
        $job = new Job([

            'description' => $request->get('name'),
        ]);
        Log::info('Request log:' . $request);
        Log::info('Get name log:' . $request->get('name'));
        $incident = Incident::find($request->get('incident_id'));
        Log::info('Incident ID log:' . $request->get('incident_id'));


        $job->incident()->associate($incident);
        Log::info('Job Log:' . $job);
        $job->save();

        //Validation against adding multiple jobs for 1 incident

        return redirect('jobs')->with('success', 'Job saved');
    }

    /*
      Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    //This will be used by contractors to update job status && Cost calls update
    //call view edit contractorJobs View here
    public function edit($id, Request $request)
    {
        Log::info($id);
        $job = Job::find($id); 
        Log::info('ID check' . $job);
        $status = Status::all(); 
        $users = User::where('level_of_access', 2)->get();
        return view('jobs.updateJob', compact('job', 'users', 'status'));
    }//edit


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    //Function used by edit JOB to update cost & status e.g. In-Progress to Complete

    public function update($id, Request $request)
    {
        Log::info('Job ID: ' .$id);
        Log::info('Status: ' .$request->get('status'));
        Log::info('User ID: ' .$request->get('user_id'));
        
        Log::info('Description From Contractor: ' .$request->get('description'));
        Log::info('Cost From Contractor:: ' .$request->get('cost'));

        $job = Job::find($id);
        Log::info('Job Check: ' . $job);
        $user = Auth::user();
        $status = Status::all();
        
        if($request->has('description')||$request->has('cost')){
            //contractor updating job
            $job->cost = $request->get('cost');
            $job->description = $request->get('description');
            $job->save();
            return redirect('/jobs');
        } else {
             //this section updates status
            $user->available = false;
            $job->user_id = $request->get('user_id');
            Log::info('User ID check Job assign' .$job->user_id);
            
           foreach($status as $status){
                if($status->id == $request->get('status')){
                    $status = $status;
                    $job->status_id = $status->id;
                }//IF
            }//for

            
            $job->save();
            return redirect('/jobs');
        }
    } //update

    public function displayPreviousJobs()
    {
        return view('jobs.displayPrevious');
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    // Not used all job completed can be viewed via reports
    public function destroy($id)
    {
        $job = Job::find($id);
        $job->delete();

        return redirect('/jobs')->with('success', 'job deleted!');
    }


    public function getIncidents()
    {
        $incidents = Incident::all();
        Log::info('DOING SOMETHING');
        return response()->json($incidents);
    }

    public function updateInformation(Request $request)
    {
        $incident = Incident::find($request->incident_id);
        $site = Site::find($incident->site_id);
        $organisation = Organisation::find($site->organisation_id);




        Log::info('[JobsController] update Information - Incident: ' . $incident);

        //   return response($incident->jsonSerialize());

        return Response::json(array(
            'incident' => $incident,
            'site' => $site,
            'organisation' => $organisation
        ));
    }
}
