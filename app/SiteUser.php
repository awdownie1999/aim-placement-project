<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteUser extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'site_id', 'user_id', 
    ];


    public function site()
    {
        return $this->belongsTo('App\Site');
    }

    public function user()
    {
        return $this->hasOne('App\User');
    }
}