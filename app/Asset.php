<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    protected $fillable = [

        'asset_code', 'name', 'cost', 'category_id',

    ];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
