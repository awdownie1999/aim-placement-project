<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incident extends Model
{
    protected $fillable = [
    
        'severity', 'cost', 'name'

    ];



    public function job()
    {
        return $this->hasOne('App\Job');
    }


    public function site(){
        return $this->belongsTo('App\Site');
    }
}
