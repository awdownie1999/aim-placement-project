<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FailedJob extends Model
{
    protected $fillable = [
    
        'connection', 'queue', 'payload', 'exception',

    ];
}
