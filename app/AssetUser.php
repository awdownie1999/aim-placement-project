<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetUser extends Model
{
    protected $fillable = [
        'assets_id', 'user_id',
    ];
}
