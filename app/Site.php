<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'organisation_id', 'location', 'contact_num',

    ];

    public function organisation()
    {
        return $this->belongsTo('App\Organisation');
    }

    public function incident()
    {
        return $this->hasMany('App\Incident');
    }

    public function siteUser()
    {
        return $this->hasMany('App\SiteUser');
    }
 

     /*
    public function organisation()
    {
        return $this->belongsTo('Organisation' , 'id');
    }

    public function siteUser()
    {
        return $this->hasMany('App\SiteUser');
    }
       */


}


