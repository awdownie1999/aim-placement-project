@extends('layouts.nav')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Add Asset To Order</h1>
        <div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
            @endif
            <form method="post" action="{{ route('assets.store') }}">
                @csrf
                <div class="form-group">
                    <label for="asset_code">Asset Code:</label>
                    <input type="text" class="form-control" name="asset_code" />
                </div>

                <div class="form-group">
                    <label for="category_id">category:</label>
                    <select name="category_id">
                        @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->type}}</option>
                        @endforeach
                    </select>
                </div>


                <div class="form-group">
                    <label for="name">name:</label>
                    <input type="text" class="form-control" name="name" />
                </div>

                <div class="form-group">
                    <label for="cost">cost:</label>
                    <input type="text" class="form-control" name="cost" />
                </div>


                <button type="submit" class="btn btn-primary">Add Asset</button>
            </form>
        </div>
    </div>
</div>
@endsection
