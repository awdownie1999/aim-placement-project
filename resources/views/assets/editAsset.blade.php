@extends('layouts.nav')
@Section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Update an Asset</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br />
        @endif
        <form method="post" action="{{ route('assets.update', $assets->id) }}">
            @method('PATCH')
            @csrf
            <div class="form-group">

                <label for="asset_code">Asset Code:</label>
                <input type="text" class="form-control" name="asset_code" value={{ $assets->asset_code }} />
            </div>

            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" class="form-control" name="name" value={{ $assets->name }} />
            </div>

            <div class="form-group">
                <label for="cost">Cost:</label>
                <input type="text" class="form-control" name="cost" value={{ $assets->cost }} />
            </div>

            <div class="form-group">
                <label for="category_id">Category:</label>
                <select name="category_id">
                    @foreach($categories as $category)
                    <option value="{{$category->id}}">{{$category->type}}</option>
                    @endforeach
                </select>
            </div>



            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
@endsection
