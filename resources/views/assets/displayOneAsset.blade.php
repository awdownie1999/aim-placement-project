@extends('layouts.nav')
@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">View an Asset</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br />
        @endif
        <form method="post" action="{{ route('assets.show', $assets->id) }}">
            @method('PATCH')
            @csrf
            <div class="form-group">

                <label for="asset_code">Asset Code:</label>
                <label for="costs">{{ $assets->asset_code }}:</label>
            </div>

            <div class="form-group">
                <label for="name">Name:</label>
                <label for="name">{{ $assets->name }}:</label>
            </div>

            <div class="form-group">
                <label for="cost">Cost:</label>
                <label for="costs">{{ $assets->cost }}:</label>
            </div>
        </form>
    </div>
</div>
@endsection
