@extends('layouts.nav')

@section('content')
<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">{{ __('Assets') }}</h1>    
  <table class="table table-striped">
    <thead>
        <tr>
          <td>Asset-Code</td>
          <td>Name</td>
          <td>Category </td>
          <td>Cost</td>
        </tr>
    </thead>
    <div>
    <a style="margin: 19px;" href="{{ route('assets.create')}}" class="btn btn-primary">New Asset</a>
    </div>  
    <tbody>
        @foreach($assets as $asset)
        @foreach($categories as $category)
        <tr>
            <td>{{$asset->asset_code}}</td>
            <td>{{$asset->name}}</td>
            <td>{{$category->type}}</td>       
            <td>{{$asset->cost}}</td>

            <td>
                <a href="{{ route('assets.edit',$asset->id)}}" class="btn btn-primary">Edit</a>
            </td>
            <td>
                <a href="{{ route('assets.show',$asset->id)}}" class="btn btn-primary">Display</a>
            </td>
            <td>
                <form action="{{ route('assets.destroy', $asset->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
                </td>
        </tr>
        @endforeach
        @endforeach
    </tbody>
  </table>

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
</div>
@endsection