<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  


  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Asset Incident Management System') }}</title>

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>

<body>


<style>
  .dropdownhover:hover>.dropdown-menu{
    display: block;
  }
</style>

<div id="app">

<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">

<div class="container">

<!--Displays the title-->
<a class="navbar-brand" href="{{ url('/') }}">
      {{ config('app.name', 'Asset Incident Management') }}
</a>
<!--Displays the title END-->

<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
      </button>

<!--Group for the buttons/Links-->
<div class="collapse navbar-collapse navbar-nav" id="navbarSupportedContent">


<!--Incident Drop Down-->
<div id="Incidents" class="mr-auto nav-item dropdownhover">

<a id="navbarDropdown" class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
{{ __('Incidents') }}<span class="caret"></span>
</a>



<!--SubButtons for IncidentButton-->
<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
  
    <a class="dropdown-item" href="incidents"
    onclick="event.preventDefault(); document.getElementById('incident-create-form').submit();">
    {{ __('Logout') }}</a>
  </div>



  <form id="incident-create-form" action="incidents" method="POST" style="display: none;">
                                        @csrf
                                    </form>


  <!--SubButtons for UserButton END-->


</div>
<!--Incident Drop Down END-->




<!--User Drop Down-->
<div id="User" class="ml-auto nav-item dropdown">


<!--User Main Button-->
<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" data-click-behavior="default" aria-haspopup="true" aria-expanded="false" v-pre >
  {{ Auth::user()->name }} <span class="caret"></span>
</a>
  <!--User Main Button END-->


  <!--SubButtons for UserButton-->
  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
    <a class="dropdown-item" href="#"
    onclick="event.preventDefault(); document.getElementById('account-form').submit();">{{ __('Account') }}
  </a>


    <a class="dropdown-item" href="{{ route('logout') }}"
    onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
    {{ __('Logout') }}</a>
  </div>



  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>

                                    <form id="account-form" action="" method="POST" style="display: none;">
                                        @csrf
                                    </form>

  <!--SubButtons for UserButton END-->

</div>
<!--User Drop Down END-->

</div>
<!--Group for the buttons/Links END-->

</div>

</nav>

</div>

</body>

</html>