<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">



    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.owner', 'ACME Industries')}}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://kit.fontawesome.com/6925b55297.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

</head>

<body>



    <div id="app">

        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">

            <div class="container">


                <div>
                    <a class="navbar-brand-custom" href="{{ url('/') }}">
                        {{ config('app.title', 'ACME')}}
                    </a>
                    <a class="navbar-brand-custom navbar-brand-custom-light" href="{{ url('/') }}">
                        {{ config('app.name', 'Asset Incident Management')}}
                    </a>

                </div>


                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        @guest

                        <!--What will display when user is not authenticated.-->



                        @else

                        <!--What will display when user is authenticated.-->
                        <!--Look at app.blade.php to see how to do drop down on buttons.-->

                        <a class="nav-link" href="/home">{{ __('Home') }}</a>
                        <a class="nav-link" href="/assets">{{ __('Assets') }}</a>
                        <a class="nav-link" href="/categories">{{ __('Categories') }}</a>
                        <a class="nav-link" href="/reports">{{ __('Reports') }}</a>


                        <li class="nav-item dropdownhover">

                            <a id="navbarDropdown" class="nav-link" href="jobs" role="button" aria-haspopup="true" aria-expanded="false">
                                {{ __('Jobs') }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                                <a class="dropdown-item" href="/incidents/create">
                                    {{ __('Assign Jobs') }}
                                </a>


                            </div>


                        </li>


                        <!--Start of a button that has sub classes on hover.-->
                        <li class="nav-item dropdownhover">
                            <a id="navbarDropdown" class="nav-link" href="/incidents" role="button" aria-haspopup="true" aria-expanded="false">
                                {{ __('Incidents') }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                                <a class="dropdown-item" href="/incidents/create">
                                    {{ __('Create') }}
                                </a>


                            </div>
                        </li>
                        <!--Start of a button that has sub classes on hover END-->






                        @endguest
                    </ul>

                    <ul class="navbar-nav ml-auto">

                        @guest

                        <!--What will display when user is not authenticated.-->

                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                        @endif

                        @else

                        <!--What will display when user is authenticated.-->

                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>

                                {{ Auth::user()->name }} <span class="caret"></span>
                                <i class="fas fa-user-circle fa-2x"></i>

                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">


                                <a class="dropdown-item" href="/account">
                                    {{ __('Account') }}
                                </a>


                                {{-- <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a> --}}




                                {{-- <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form> --}}


                            </div>

                        </li>
                        @endguest
                    </ul>
                </div>

            </div>

        </nav>

        <main class="py-4">
            @yield('content')
        </main>

    </div>

</body>

</html>
