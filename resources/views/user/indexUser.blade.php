@extends('layouts.nav')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <h1 class="display-3">Users</h1>
        <table class="table table-striped">
            <thead>
                <tr>
                    <td>Name</td>
                    <td>Email </td>
                    <td>Address</td>
                    <td>Address_two</td>
                    <td>Contact_num </td>
                    <td>Contact_two</td>
                    <td>Level of Access</td>
                    <td>Available</td>
                    <td>Hours </td>
                </tr>
            </thead>
            <div>
                <a style="margin: 19px;" href="{{ route('users.create')}}" class="btn btn-primary">New User</a>
            </div>
            <tbody>
                @foreach($users as $user)
                <tr>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->cost}}</td>
                    <td>{{$user->address}}</td>
                    <td>{{$user->address_two}}</td>
                    <td>{{$user->contact_num}}</td>
                    <td>{{$user->contact_two}}</td>
                    <td>{{$user->available}}</td>
                    <td>{{$user->hours}}</td>

                    <td>
                        <a href="{{ route('users.edit',$user->id)}}" class="btn btn-primary">Edit</a>
                    </td>
                    <td>
                        <form action="{{ route('users.destroy', $user->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        @if(session()->get('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
        @endif
    </div>
</div>
@endsection
