@extends('layouts.nav')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Add a User</h1>
        <div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
            @endif
            <form method="post" action="{{ route('users.store') }}">
                @csrf
                <div class="form-group">
                    <label for="name">name:</label>
                    <input type="text" class="form-control" name="name" />
                </div>

                <div class="form-group">
                    <label for="email">email:</label>
                    <input type="text" class="form-control" name="email" />
                </div>

                <div class="form-group">
                    <label for="pwd">password:</label>
                    <input type="text" class="form-control" name="password" />
                </div>

                <div class="form-group">
                    <label for="address">address:</label>
                    <input type="text" class="form-control" name="address" />
                </div>

                <div class="form-group">
                    <label for="address_two">address 2:</label>
                    <input type="text" class="form-control" name="addressTwo" />
                </div>

                <div class="form-group">
                    <label for="contact_num">contact num:</label>
                    <input type="text" class="form-control" name="contact_num" />
                </div>

                <div class="form-group">
                    <label for="contact_two">contact num2:</label>
                    <input type="text" class="form-control" name="contact_two" />
                </div>

                <div class="form-group">
                    <label for="user_role">Role:</label>
                    </br>
                    <select name="user_role">
                        <option name="Admin">Admin</option>
                        <option name="Task Manager">Task Manager</option>
                        <option name="Contractor">Contractor</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="available">available:</label>
                    <input type="text" class="form-control" name="available" />
                </div>

                <div class="form-group">
                    <label for="hours">hours:</label>
                    <input type="text" class="form-control" name="hours" />
                </div>

                <button type="submit" class="btn btn-primary">Add User</button>
            </form>
        </div>
    </div>
</div>
@endsection
