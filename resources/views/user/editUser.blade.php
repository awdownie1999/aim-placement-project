@extends('layouts.nav')
@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Update a user</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br />
        @endif
        <form method="post" action="{{ route('users.update', $user->id) }}">
            @method('PATCH')
            @csrf
            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" class="form-control" name="name" value="{{ $user->name }}" />
            </div>

            <div class="form-group">
                <label for="email">email:</label>
                <input type="text" class="form-control" name="email" value="{{ $user->email }}" />
            </div>

            <div class="form-group">
                <label for="password">password:</label>
                <input type="text" class="form-control" name="password" value="{{ $user->password }}" />
            </div>

            <div class="form-group">
                <label for="address">address:</label>
                <input type="text" class="form-control" name="address" value="{{ $user->address }}" />
            </div>

            <div class="form-group">
                <label for="address_two">address 2:</label>
                <input type="text" class="form-control" name="address_two" value="{{ $user->address_two }}" />
            </div>

            <div class="form-group">
                <label for="contact_num">contact num:</label>
                <input type="text" class="form-control" name="contact_num" value="{{ $user->contact_num }}" />
            </div>

            <div class="form-group">
                <label for="contact_num_two">contact num2:</label>
                <input type="text" class="form-control" name="contact_num_two" value="{{ $user->contact_num_two }}" />
            </div>

            <div class="form-group">
                <label for="level_of_access">Level Of Access:</label>
                <input type="text" class="form-control" name="level_of_access" value="{{ $user->level_of_access }}" />
            </div>

            <div class="form-group">
                <label for="available">available:</label>
                <input type="text" class="form-control" name="available" value="{{ $user->available }}" />
            </div>

            <div class="form-group">
                <label for="hours">hours:</label>
                <input type="text" class="form-control" name="hours" value="{{ $user->hours }}" />
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
@endsection
