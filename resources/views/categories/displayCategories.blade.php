@extends('layouts.nav')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <h1 class="display-3">{{ __('Categories') }}</h1>

        <table class="table table-striped">
            <thead>
                <tr>

                    <td>{{ __('Category Name') }}</td>
                </tr>
            </thead>
    </div>
    <div>
        <a style="margin: 19px;" href="{{ route('categories.create')}}" class="btn btn-primary">New Category</a>
    </div>
</div>
<tbody>
    @foreach($categories as $category)
    <tr>
        <td>{{$category->type}}</td>
        <td><a href="{{ route('categories.edit',$category->id)}}" class="btn btn-primary">Edit Category</a></td>
        <td>
            <form action="{{ route('categories.destroy', $category->id)}}" method="post">
                @csrf
                @method('DELETE')
                <button class="btn btn-danger" type="submit">Delete</button>
            </form>
        </td>
    </tr>

    @endforeach
</tbody>
</table>

</div>
</div>
@endsection
