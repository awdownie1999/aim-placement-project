@extends('layouts.nav') 
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">View an Incident</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br /> 
        @endif
        <form method="post" action="{{ route('incidents.show', $incidents->id) }}">
            @method('PATCH') 
            @csrf
            <div class="form-group">

                <label for="severity">Severity:</label>
                <label for="severitys">{{ $incidents-> severity }}:</label>
            </div>

            <div class="form-group">
                <label for="cost">Cost:</label>
                <label for="cost">{{ $incidents-> cost }}:</label>
            </div>

            <div class="form-group">
                <label for="job_complete">Job Complete:</label>
                <label for="job_complete">{{ $incidents-> job_complete}}:</label>
            </div>
        </form>
    </div>
</div>