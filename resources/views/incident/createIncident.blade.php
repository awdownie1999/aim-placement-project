@extends('layouts.nav')

@section('content')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Add a Incident</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
    
      <form method="post" action="{{ route('incidents.store') }}">
          @csrf
          
          <div class="form-group">    
              <label for="organisation_id">Organisation</label>
             
              <select name="organisation_id">
              @foreach($organisations as $organisation)
                <option value="{{$organisation->id}}">{{$organisation->name}}</option> 
              @endforeach
                </select> 
          </div>
{{--TO DO--}}
{{-- Lift in Organisation ID based off user }}
{{--JS: Make Sites auto populate based of organisation_id--}}

          <div class="form-group">    
              <label for="site_id">Site</label>
              <select name="site_id">
              @foreach($sites as $site)
                <option value="{{$site->id}}">{{$site->location}}</option>
                @endforeach
                </select> 
          </div>
          
          <div class="form-group">
              <label for="name">Name:</label>
              <input type="text" class="form-control" name="name"/>
          </div>

          <div class="form-group">
              <label for="severity">Severity:</label>
              <input type="text" class="form-control" name="severity"/>
          </div>


<button type="submit" href="{{ route('incidents.store', ['organisation_id'=>$site->organisation->id, 'site_id'=>$site->id]) }}" class="btn btn-primary-outline">Add Incident</button>
      </form>
  </div>
</div>
</div>
@endsection
