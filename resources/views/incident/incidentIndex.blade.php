@extends('layouts.nav')

@section('content')
<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">Incident</h1>    
  <table class="table table-striped">
    <thead>
        <tr>
          <td>Organisation</td>
          <td>Severity</td>
          <td>Name</td>
        </tr>
    </thead>
    <div>
    <a style="margin: 19px;" href="{{ route('incidents.create')}}" class="btn btn-primary">New Incident</a>
    </div>  
    <tbody>
        @foreach($incidents as $incident) 
         <tr>
        
        <td>{{$incident->site->organisation->name}} </td>
      
        {{--Make this display actual org for incidents--}}

            <td>{{$incident->severity}}</td>
            <td>{{$incident->name}}</td>
            <td>
                <a href="{{ route('incidents.edit',$incident->id)}}" class="btn btn-primary">Edit</a>
            </td>
            <td>
                <a href="{{ route('incidents.show',$incident->id)}}" class="btn btn-primary">Display</a>
            </td>
            <td>
                <form action="{{ route('incidents.destroy', $incident->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
                </td>
        </tr>
        @endforeach
    </tbody>
  </table>

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
</div>
@endsection