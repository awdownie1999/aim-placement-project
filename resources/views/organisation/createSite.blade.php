@extends('layouts.nav')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Create Site</h1>
        <div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
            @endif

            <form method="post" action="{{ route('sites.store') }}">

                @csrf
                <div class="form-group">
                    <label for="organisation_id">Organisation</label>
                    <select name="organisation_id">
                        @foreach($organisations as $organisation)
                        <option value="{{$organisation->id}}">{{$organisation->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="location">Location</label>
                    <input type="text" class="form-control" name="location" />
                </div>

                <div class="form-group">
                    <label for="contact_num">Contact Number:</label>
                    <input type="text" class="form-control" name="contact_num" />
                </div>

                <div class="form-group">
                    <label for="manager_contact_number">Manager Number:</label>
                    <input type="text" class="form-control" name="manager_contact_number" />
                </div>

                <button type="submit" href="{{ route('sites.store', ['organisation_id'=>$organisation->id]) }}" class="btn btn-primary-outline">Add Site</button>
            </form>
        </div>
    </div>
</div>
@endsection
