@extends('layouts.nav')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <h1 class="display-3">Sites</h1>
        <table class="table table-striped">
            <thead>
                <tr>
                    <td>Organisation</td>
                    <td>Location</td>
                    <td>Number</td>
                    <td>Manager Number</td>
                </tr>
            </thead>

            <tbody>
                @foreach($sites as $site)
                <tr>
                    <td>{{$organisation->name}}</td>
                    <td>{{$site->location}}</td>
                    <td>{{$site->contact_num}}</td>
                    {{--<td>{{$user->contact_number}}</td>--}}
                    <td> <a style="margin: 19px;" href="{{ route('sites.edit',$site->id)}}" class="btn btn-primary">Edit Site</a> </td>
                    <td>
                        <form action="{{ route('sites.destroy', $site->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>

        @if(session()->get('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
        @endif
    </div>
</div>
@endsection
