@extends('layouts.nav')

@section('content')
<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">Organisation</h1>    
  <table class="table table-striped">
    <thead>
        <tr>
          <td>Name</td>
          <td>Type</td>
        </tr>
    </thead> 
    </div>  
              <div>
              <a style="margin: 19px;" href="{{ route('organisation.create')}}" class="btn btn-primary">New Organisation</a>
              <a style="margin: 19px;" href="{{ route('sites.create')}}" class="btn btn-primary">New Sites</a>
              </div>  
        </div> 
    <tbody>
        @foreach($organisations as $organisation)
        <tr>
            <td>{{$organisation->name}}</td>
            <td>{{$organisation->type}}</td>
            <td><a href="{{ route('organisation.edit',$organisation->id)}}" class="btn btn-primary">Edit Organisation</a></td>
            <td>
                <form action="{{ route('organisation.destroy', $organisation->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
                </td>
            <td> <a style="margin: 19px;" href="{{ route('sites.index', ['organisation_id'=>$organisation->id]) }}" class="btn btn-primary">Display Sites</a> </td>
        </tr>

        @endforeach
    </tbody>
  </table>

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
</div>
@endsection