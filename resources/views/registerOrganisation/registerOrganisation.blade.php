@extends('nav')

@section('main')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Jobs</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
    <form method="post" action="{{ route('organisation.store') }}">
  <div class="container">
    <hr>

    <label for="name"><b>name</b></label>
    <input type="text" placeholder="Enter name" name="name" required>

    <label for="type"><b>Type</b></label>
    <input type="text" placeholder="Enter Type" name="type" required>


    <div class="clearfix">
      <button type="button" class="cancelbtn">Cancel</button>
      <button type="submit" class="signupbtn">Sign Up</button>
    </div>
  </div>
</form> 

@endsection