<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>hello</title>
</head>

<body>
    <div id="app">
        <welcome></welcome>
    </div>
    <script src="{{ mix('js/app.js') }}"></script>
</body>

</html>
