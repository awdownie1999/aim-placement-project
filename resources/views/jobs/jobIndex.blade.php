@extends('layouts.nav')

@section('content')
<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">Jobs</h1>    
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Organisation</td>
          <td>Location</td>
          <td>Severity</td>
          <td>Inital Description</td>
          <td>Description</td>
          <td>Date</td>
          <td>Cost</td>
          <td>Status</td>
        </tr>
    </thead>
    @if(Auth::user()->level_of_access == 3 || Auth::user()->level_of_access == 4)
                     
    <a style="margin: 19px;" href="{{ route('jobs.index', ['status'=> 'assigned']) }}" class="btn btn-primary">Jobs In Progress</a>

                <a style="margin: 19px;" href="{{ route('jobs.index', ['status'=> 'unassigned']) }}" class="btn btn-primary">Jobs to Assign</a>

                <a style="margin: 19px;" href="{{ route('jobs.index', ['status'=> '']) }}" class="btn btn-primary">All Jobs</a>

    <a style="margin: 19px;" href="{{ route('jobs.create')}}" class="btn btn-primary">Create Job</a>
    @endif
    <tbody>
@foreach ($jobs as $job) 
<tr>
      <td>{{$job->id}}</td>
      <td>{{$job->incident->site->organisation->name}}</td>
      <td>{{$job->incident->site->location}} </td>
      <td>{{$job->incident->severity}} </td>
      <td>{{$job->incident->name}} </td>
      <td>{{$job->description}} </td>
      <td>{{$job->date}} </td>
      <td>{{$job->cost}} </td>
      <td>{{$job->status->type}} </td>
      @if(Auth::user()->level_of_access == 3 || Auth::user()->level_of_access == 4)
      <td><form action="{{ route('jobs.destroy', $job->id)}}" method="post">
          @csrf
          @method('DELETE')
          <button class="btn btn-danger" style="margin: 19px;" type="submit">Delete</button>
      </form></td>
      @endif
      <td><a style="margin: 19px;" href="{{ route('jobs.edit', [$job->id, 'status'=>$job->status] )}}" class="btn btn-primary">Update Status</a></td>

</tr>
@endforeach
    </tbody>
  </table>

                        <td>{{$job->incident->site->organisation->name}}</td>
                        <td>{{$job->incident->site->location}} </td>
                        <td>{{$job->incident->severity}} </td>
                        <td>{{$job->incident->name}} </td>
                        <td>{{$job->description}} </td>
                        <td>{{$job->date}} </td>
                        <td>{{$job->cost}} </td>
                        <td>{{$job->status}} </td>
                        <td>

                        <td>
                            <form action="{{ route('jobs.destroy', $job->id)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>
                        </td>

                        <td><a style="margin: 19px;" href="{{ route('jobs.edit', [$job->id, 'status'=>$job->status] )}}" class="btn btn-primary"></a>Update Status</td>
                    </tr>
                    @endforeach
                </tbody>
        </table>

        @if(session()->get('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
        @endif
    </div>
</div>
@endsection
