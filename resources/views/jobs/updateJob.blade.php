@extends('layouts.nav')

@section('content')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Assign Jobs</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
      <br />
    @endif
   <form method="post" action="{{ route('jobs.update', $job->id, 'user_id', 'status', 'description', 'cost')  }}">
   @method('PATCH')
          @csrf 
          @if(Auth::user()->level_of_access == 3 || Auth::user()->level_of_access == 4)
         
          <div class="form-group">
        <label for="job_id">Job ID:</label>
        <label for="job_id">{{ $job->id }}</label>
        </div>

          <div class="form-group">
        <label for="description">Job Name:</label>
        <label for="description">{{ $job->incident->name }}</label></div>

        <div class="form-group">
        <label for="severity">Severity:</label>
        <label for="severity">{{ $job->incident->severity}}</label></div>

        <div class="form-group">
        <label for="organisation_name">Organisation Name</label>
        <label for="organisation_name">{{$job->incident->site->organisation->name}}</label></div>

          <div class="form-group">
          <label for="location">Location</label>
          <label for="location">{{$job->incident->site->location}}</label></div>

          <div class="form-group">
          <label for="user_id">Contractor:</label>
          <select name ="user_id">
          @foreach ($users as $user)
          <option value="{{$user->id}}">{{$user->name}}</option> 
          @endforeach
          </select>
          </div>

          <div class="form-group">
          <label for="status">Status:</label>
          <select name ="status">
          @foreach ($status as $status)
          <option value="{{$status->id}}">{{$status->type}}</option> 
          @endforeach
          </select>
          
          </div>

          @else 
          <table class="table table-striped">
    <thead>
    <td>Job ID</td>
    <td>Organisation Name </td>
    <td>Location </td>
    <td>Initial Description</td>
    <td>Updated Description</td>
    <td>Cost</td>
    </thead>

    <tbody> 
    <td>{{$job->id }}</td>
    <td>{{$job->incident->site->organisation->name}} </td>
    <td>{{$job->incident->site->location}} </td>
    <td>{{$job->incident->name}} </td>
    <td>{{$job->description}} </td>
    <td>{{$job->cost}} </td>

    </tbody>
    </table>

    <div class="form-group">
    <label for="description">Description:</label>
    <input type="text" class="form-control" name="description" value='{{ $job->description }}' />
    </div>

    <div class="form-group">
    <label for="cost">Cost:</label>
    <input type="text" class="form-control" name="cost" value='{{ $job->cost }}' />
    </div>
    <button type="submit"  class="btn btn-primary-outline">Complete</button> 
    
          @endif
            <button type="submit"  class="btn btn-primary-outline">Update</button> 
      </form>

      

</div>
</div>
@endsection