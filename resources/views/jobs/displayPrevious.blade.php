@extends('layouts.nav')

@section('content')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Job</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
      <br />
    @endif     
                  
          {{--If any job where status==complete && has user_id == AuthUser display table ...}}
        
        <h2 class="display-3">Previous Jobs</h2>
    <table class="table table-striped">
    <thead>

    </thead>
            {{--Previous Jobs:
            organisation->name
            site->location
            job->description
            user->hours
            job->date--}}
    <tbody> 
    
    </tbody>
    </table>


    </div>
</div>
@endsection