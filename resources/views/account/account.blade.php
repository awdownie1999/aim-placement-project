@extends('layouts.nav')


@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Account') }}</div>

                <div class="card-body">



                    <table style="width: 100%;">
                        <tr>
                            <td style="text-align: left;">
                                <ul style="list-style-type: none;">

                                    <li>{{ __('Name') }} : {{ Auth::user()->name }}</li>

                                    <li>{{ __('Address') }} : {{ Auth::user()->address }}</li>

                                    <li>{{ __('Second Address') }} : {{ Auth::user()->address_two }}</li>

                                    <li>{{ __('Contact Number') }} : {{ Auth::user()->contact_num }}</li>

                                    <li>{{ __('Second Contact Number') }} : {{ Auth::user()->contact_two }}</li>

                                    <li>{{ __('Role') }} : {{$role}}</li>



                                    @if(strcasecmp($role, "client") == 0)
                                    @if($site == null)

                                    <label>There is no Job Location Set, set one using the button below:</label>

                                    <br />
                                    <a class="btn btn-secondary" href="{{ route('account.edit', $user->id)}}">
                                        {{ __('Set Job Location') }}
                                    </a>

                                    @else

                                    <br />
                                    <li style=" text-decoration: underline;">Job Location Information</li>


                                    <li>{{__('Organisation')}} : {{$organisation->name}}</li>
                                    <li>{{__('Site')}} : {{$site->location}}</li>
                                    <li>{{__('Site Contact Number')}} : {{$site->contact_num}}</li>


                                    <br />
                                    <a class="btn btn-secondary" href="{{ route('account.edit', $user->id)}}">

                                        {{ __('Change Job Location') }}
                                    </a>
                                    <button class="btn btn-secondary" href="">
                                        {{ __('Remove Job Location') }}
                                    </button>




                                    @endif



                                    @endif





                                </ul>
                            </td>

                            <td style=" text-align: right;">
                                <i class="fas fa-user-circle fa-7x"></i>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> @endsection
