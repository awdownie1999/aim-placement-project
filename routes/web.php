<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view(
        'welcome',
        [
            'title' => "An even cooler way to do the title"
        ]
    );
});

/*
    Route::get('home', function ()
    {
        return view('home');
    })->middleware('auth');
*/


//This is used to check if pages can be views by non authenticated users.
Route::group(['middleware' => ['auth']], function () {
    Route::resource('assets', 'AssetController');
    Route::resource('home', 'HomeController');
    Route::resource('jobs', 'JobsController');
    Route::resource('incidents', 'IncidentController');

    Route::resource('organisation', 'OrganisationController');
    Route::resource('sites', 'SiteController');
    Route::resource('users', 'UserController');
    Route::resource('userPaid', 'UserController');
    Route::resource('categories', 'CategoryController');
    Route::resource('account', 'AccountController');
});



//Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');

//Route::get('/register', 'Auth\RegisterController@index')->name('register');



/* Testing */

Route::get('/page', function () {
    return view(
        'testing.page',
        [
            'title' => "Page 2 - A little about the Author",
            'author' => json_encode([
                "name" => "Fisayo Afolayan",
                "role" => "Software Enginner",
                "code" => "Always keeping it clean"
            ])
        ]
    );
});


Route::get('/spa', function () {
    return view('testing.vueapp');
})->where('any', '.*');
