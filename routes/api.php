<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('getOrganisations', 'AccountController@getOrganisations');
Route::get('getSites', 'AccountController@getSites');
Route::get('getIncidents', 'JobsController@getIncidents');
Route::get('updateJobInformation', 'JobsController@updateInformation');
Route::get('updateSites', 'AccountController@updateSites');
