<?php
use Illuminate\Database\Seeder;

use App\Asset;

class PassworkResetsSeeder extends Seeder
{
    public function run()
    {
        $sampleAsset = PasswordReset::firstOrNew([
            'email' => 'admin@example.com',
        ]);

        $sampleAsset->fill([
            'token' => true
        ]);
        save();
    } 
}