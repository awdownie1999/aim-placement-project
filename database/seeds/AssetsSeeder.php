<?php

use Illuminate\Database\Seeder;

use App\Asset;

class AssetsSeeder extends Seeder
{
    public function run()
    {
        $sampleAsset = Asset::firstOrNew([
            'asset_code' => 1,
            'name' => 'Wash',
            'cost' => '100',
            'category_id' => '2'
        ]);
    }
}
