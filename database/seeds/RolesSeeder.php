<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sampleClient = Role::firstOrNew([
            'user_role' => "Client",
        ]);

        $sampleClient->save();


        $sampleContractor = Role::firstOrNew([
            'user_role' => "Contractor",
        ]);

        $sampleContractor->save();

        $sampleTM = Role::firstOrNew([
            'user_role' => "Task Manager",
        ]);

        $sampleTM->save();

        $sampleAdmin = Role::firstOrNew([
            'user_role' => "Admin",
        ]);

        $sampleAdmin->save();
    }
}
