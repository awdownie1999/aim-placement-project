<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Role;


class UsersSeeder extends Seeder
{
    public function run()
    {
        $sampleAdmin = User::firstOrNew([
            'email' => "admin@example.com",
        ]);

        // **** This uses eloquent model *** \\

        $sampleAdmin->fill([
            'name' => 'Admin User',
            'password' => Hash::make('password'),
            'address' => '15 Carolina Street',
            'address_two' => '43 Nery Road',
            'contact_num' => '06286348762',
            'contact_two' => '08825443343',
            'level_of_access' => 4,
            'available' => NULL,
            
        ]);
        $sampleAdmin->save();


        $sampleJordan = User::firstOrNew([
            'email' => "jordanellison863@gmail.com",
        ]);

        // **** This uses eloquent model *** \\

        $sampleJordan->fill([
            'name' => 'Jordan Ellison',
            'password' => Hash::make('password'),
            'address' => '15 Carolina Street',
            'address_two' => '43 Nery Road',
            'contact_num' => '06286348762',
            'contact_two' => '08825443343',
            'level_of_access' => 1,
            'available' => NULL,
           
        ]);
        $sampleJordan->save();

        /*
        if (!$sampleAdmin->hasRole('administrator')) {
            $sampleAdmin->assignRole('administrator');
        }
        */


        $sampleTaskMan = User::firstOrNew([
            'email' => "taskman@example.com",
        ]);

        $sampleTaskMan->fill([
            'name' => 'TaskMan User',
            'password' => Hash::make('password'),
            'address' => '15 Carolina Street',
            'address_two' => '43 Nery Road',
            'contact_num' => '06286348762',
            'contact_two' => '08825443343',
            'level_of_access' => 3,
            'available' => NULL,
           
        ]);
        $sampleTaskMan->save();



        $sampleContractor = User::firstOrNew([
            'email' => "contract@example.com",
        ]);

        $sampleContractor->fill([
            'name' => 'Contractor User',
            'password' => Hash::make('password'),
            'address' => '2 Contract Way',
            'contact_num' => '06286348762',
            'level_of_access' => 2,
            'available' => true,
           
        ]);
        $sampleContractor->save();





    }
}
