<?php

use Illuminate\Database\Seeder;

use App\Organisation;

class OrganisationsSeeder extends Seeder
{
    public function run()
    {

        DB::table('organisations')->insert([
            'name' => Str::random(10),
            'type' => Str::random(10),
        ]);

        /*$organisation = Organisation::firstOrNew([
            'name' => "Russell's",
        ]);

        // **** This uses eloquent model *** \\

        $organisation->fill([
            'type' => "Shop"
        ]);
        $organisation->save();*/  
    }
}