<?php

use Illuminate\Database\Seeder;
use App\Site;

class SiteSeeder extends Seeder
{
    public function run()
    {

        $sites = Site::firstOrNew([
            'location' => "Belfast",

        ]);

        // **** This uses eloquent model *** \\

        $sites->fill([
            'organisation_id' => "1",
            'contact_num' => "12345678901"
        ]);
        $sites->save(); #


        $sites_two = Site::firstOrNew([
            'location' => "Shankill",

        ]);

        // **** This uses eloquent model *** \\

        $sites_two->fill([
            'organisation_id' => "1",
            'contact_num' => "65678987654"
        ]);
        $sites_two->save();
    }
}
