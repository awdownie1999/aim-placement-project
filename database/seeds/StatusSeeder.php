<?php

use Illuminate\Database\Seeder;
use App\Status;

class StatusSeeder extends Seeder
{
    public function run()
    {
        $unassigned = Status::firstOrNew([ 
            'type' => 'unassigned',
        ]);

        $unassigned->save();



        $assigned = Status::firstOrNew([ 
            'type' => 'assigned',
        ]);

        $assigned->save();



        $complete = Status::firstOrNew([ 
            'type' => 'complete',
        ]);

        $complete->save();
    }
}
