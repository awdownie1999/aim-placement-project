<?php
use Illuminate\Database\Seeder;

use App\FailJob;

class FailedJobsSeeder extends Seeder
{
    public function run()
    {
        $sampleAsset = FailedJob::firstOrNew([
            'connection' => 'Test Refridgerator',
        ]);

        $sampleAsset->fill([
            'queue' => '',
            'payload' => '',
            'exception' => ''
        ]);
    } 
}