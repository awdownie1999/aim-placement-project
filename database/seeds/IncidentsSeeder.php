<?php

use Illuminate\Database\Seeder;

use App\Incident;

class IncidentsSeeder extends Seeder
{
    public function run()
    {

        /*DB::table('organisations')->insert([
            'severity' => Str::random(10),
            'cost' => Str::random(10),
            'job_complete' => Str::random(10),
        ]);*/

        $incidents = incident::FirstOrNew([
            'id' => "1",
            'severity' => "ASAP",
            'site_id' => "1",
            'name' => "Broken Fridge",
        ]);

        // **** This uses eloquent model *** \\
        $incidents->save();


        $incidents2 = incident::FirstOrNew([
            'id' => "2",
            'severity' => "ASAP",
            'site_id' => "1",
            'name' => "Broken Toy",
        ]);

        $incidents2->save();
    }
}
