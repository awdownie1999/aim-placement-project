<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //These need to go in a cetain order to make sure things are populated before they get called.
        $this->call([
            RolesSeeder::class,
            OrganisationsSeeder::class,
            SiteSeeder::class,
            CategoriesSeeder::class,
            AssetsSeeder::class,
            IncidentsSeeder::class,
            UsersSeeder::class,
            StatusSeeder::class,
            JobsSeeder::class,
            SiteUserSeeder::class,
            /*
        PassworkSeeder::class,
        FailedJobsSeeder::class,*/
        ]);
    }
}
