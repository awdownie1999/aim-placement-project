<?php

use Illuminate\Database\Seeder;
use App\SiteUser;

class SiteUserSeeder extends Seeder
{
    public function run()
    {
        $sites = SiteUser::firstOrNew([
            'user_id' => 2,
        ]);

        $sites->fill([
            'site_id' => 1,
        ]);

        $sites->save();
    }
}
