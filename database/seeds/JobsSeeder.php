<?php
use Illuminate\Database\Seeder;

use App\Job;

class JobsSeeder extends Seeder
{
    public function run()
    {

        /*DB::table('jobs')->insert([
            'user_id' => Str::random(10),
            'incident_id' => Str::random(10),
        ]);*/

        $jobs = Job::FirstOrNew([
            'id' => "1",
            'incident_id'=> '1',
            'status_id'=> '1',
            'user_id'=> '4',
            'description' => 'broken temperature control on fridge',
            'cost' => '50',
            ]);

        // **** This uses eloquent model *** \\
        $jobs->save();

    } 
}