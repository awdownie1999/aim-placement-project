<?php
use Illuminate\Database\Seeder;

use App\Certification;

class CertificationsSeeder extends Seeder
{
    public function run()
    {
        $sampleAsset = Certification::firstOrNew([
            'user_id' => 1,
        ]);

        $sampleAsset->fill([
            'certification' => 'electrician',
            'years' => 4
        ]);
    } 
}