<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('assets_id')->unsigned();//foreign
            $table->foreign('assets_id')->references('id')->on('assets')->onDelete('cascade');

            $table->integer('user_id')->unsigned();//foreign
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('asset_users');
    }
}
